/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da
#include<string.h> //Biblioteca para uso da fun��o strlen()
#define TAM 20 //Criando uma constante de tamanho 20. Constante Simb�lica

//Bloco de c�digo principal
main () {
	//Declara��o de vari�veis
	unsigned int idade; //unsigned informa ao compilador que a vari�vel usar� somente n�meros positivos
	float salario;
	char nome[TAM]; //Vetor de caracteres com 25 posi��es
	int str_size;
	
	//Entrada de dados
	printf("Digite o nome do usuario: ");
	fgets(nome, TAM, stdin); //A fun��o fgets � a melhor escolha para leitura de string. Ela impede a leitura de mais caracteres do que o permitido
	
	fflush(stdin); //Fun��o usada para limpar o buffer do teclado
	
	printf("Digite a idade do usuario: ");
	scanf("%u", &idade);
	
	printf("Digite o salario do usuario: ");
	scanf("%f", &salario);
	
	putchar('\n'); //Imprime apenas um caracter na tela
	
	//Obtendo tamanho da palavra digitada pelo usu�rio
	str_size = strlen(nome);
	
	//Removendo o '\n' da string e adicionando o caractere delimitador('\0') no lugar
	if (nome[str_size - 1] == '\n')
		nome[str_size - 1] = '\0';
	
	//Sa�da de dados
	printf("Nome: %s\nIdade: %u\nSalario: %.2f\n\n", nome, idade, salario);
}
