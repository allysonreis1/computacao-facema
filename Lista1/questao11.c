/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	float salario, reajuste;
	
	printf("Digite o salario: ");
	scanf("%f", &salario);
	
	/*Multiplicar por 1.1 implica que os 10% ser�o acrescidos do valor do salario, sem a necessidade
	 *de fazer o c�lculo com 0.1 e depois somar esse valor ao sal�rio inicial*/
	reajuste = salario*1.1; 
	
	printf("O salario reajustado com 10%% de desconto e: R$ %.2f\n\n", reajuste);
}
