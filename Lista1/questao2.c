/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	int numero1, numero2, numero3, soma;
	
	printf("Digite o primeiro numero: ");
	scanf("%d", &numero1);
	
	printf("Digite o segundo numero: ");
	scanf("%d", &numero2);
	
	printf("Digite o terceiro numero: ");
	scanf("%d", &numero3);
	
	soma = numero1 + numero2 + numero3;
	
	printf("O quadrado da soma de %d, %d e %d: %d\n", numero1, numero2, numero3, soma*soma);
}
