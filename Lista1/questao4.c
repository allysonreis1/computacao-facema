/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	float numero;
	
	printf("Digite um numero: ");
	scanf("%f", &numero);
	
	//Estrutura condicional
	if (numero > 0) 
		printf("O numero %.2f e maior que zero!", numero);
	else if (numero < 0)
		printf("O numero %.2f e menor que zero!", numero);
	else 
		printf("O numero %.2f e igual que zero!");
}
