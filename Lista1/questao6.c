/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados
#include<math.h> //Biblioteca para uso da fun��o pow()

main () {
	float numero, resultado;
	
	do {
		printf("Insira um numero: ");
		scanf("%f", &numero);
		
		if (numero < 0 )
			puts("Insira um numero positivo");
	} while (numero < 0);
	
	resultado = pow(numero, 2); //A fun��o pow eleva um valor a outro. Retornando um resultado
	printf("%.2f ao quadrado e = %.2f", numero, resultado);
}
