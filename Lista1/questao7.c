/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	float nota1, nota2, media;
	
	printf("Digite a nota1 do aluno: ");
	scanf("%f", &nota1);
	
	printf("Digite a nota2 do aluno: ");
	scanf("%f", &nota2);
	
	media = (nota1 + nota2)/2;
	
	if (media >= 7.0)
		printf("Aprovado com media %.2f!\n", media);
	else if (media >= 4.0 && media < 7.0) 
		printf("Prova final com media %.2f!\n", media);
	else 
		printf("Reprovado com media %.2f!\n", media);
}
