/*Aluno: Allyson Reis Vieira de Aguiar 16133686
* Lista 01 --- Facema - Computa��o II
* Meu reposit�rio: https://gitlab.com/allysonreis1/computacao-facema
*/

#include<stdio.h> //Biblioteca de entrada e sa�da de dados

main () {
	int diaNascimento, mesNascimento, anoNascimento, idade;
	int diaAtual, mesAtual, anoAtual;
	
	printf("Insira o a data de hoje(dd/mm/aaaa): ");
	scanf("%d/%d/%d", &diaAtual, &mesAtual, &anoAtual);
	
	printf("Insira o sua data de nascimento(dd/mm/aaaa): ");
	scanf("%d/%d/%d", &diaNascimento, &mesNascimento, &anoNascimento);
	
	idade =  anoAtual - anoNascimento;
	
	if (idade >= 18)
		printf("Usuario maior de idade: %d anos\n", idade);
}
